import request from '@/utils/request'

export function getCategorySelect() {
    return request({
        url: '/article_categories/getSelectLists',
        method: 'get'
    })
}

export function getList(params, get_url = false) {
    var url = `/article_categories`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function create(data, get_url = false) {
    var url = `/article_categories/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/article_categories/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/article_categories/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/article_categories/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
