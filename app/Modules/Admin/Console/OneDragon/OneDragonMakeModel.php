<?php

namespace App\Modules\Admin\Console\OneDragon;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OneDragonMakeModel extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:one-dragon-model {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new one-dragon-model.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Entites';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return dirname(__DIR__) . '/stubs/model.plain.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace ( $rootnamespace )
    {
        return 'App\Modules\Admin\Entities';
    }
}
