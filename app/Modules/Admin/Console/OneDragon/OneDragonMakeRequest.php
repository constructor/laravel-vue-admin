<?php

namespace App\Modules\Admin\Console\OneDragon;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OneDragonMakeRequest extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:one-dragon-request {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new one-dragon-request.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Requests';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return dirname(__DIR__) . '/stubs/request.plain.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace ( $rootnamespace )
    {
        return 'App\Modules\Admin\Http\Requests';
    }
}
